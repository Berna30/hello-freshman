import { Component } from "react";
//imported routes
import HomeView from "./CustomComponents/HomeView";
import AddMaterial from "./CustomComponents/AddMaterial";
import AddStudyGroup from "./CustomComponents/AddStudyGroup";
import LoginView from "./CustomComponents/LoginView";
import StudyGroup from "./CustomComponents/StudyGroup";
import SignupView from "./CustomComponents/SignupView";
import Materials from "./CustomComponents/Materials";
import Profile from "./CustomComponents/Profile";
import MaterialRequests from "./CustomComponents/MaterialRequests";
import JoinsView from "./CustomComponents/JoinsView";
import EditProfile from "./CustomComponents/EditProfile";
import UsersMaterials from "./CustomComponents/UsersMaterials";
import axios from "axios";

axios.defaults.withCredentials = true;

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response && error.response.status === 401) {
      console.log("Unauthorized. Redirecting to login page.");
    }
    return Promise.reject(error);
  }
);

class App extends Component {
  //constructor defined
  constructor(props) {
    super(props);
    this.state = {
      currentPage: "none",
      novicaId: 0,
      isLoggedIn: false,
      userStatus: null,
    };
  }

  componentDidMount(){
    axios.get('http://88.200.63.148:5015/User/login')
    .then(response=>{
      console.log(response)
      this.setState({userStatus:response.data})
    })
   }
   

   QSetUser=(obj)=>{
    this.setState({
      isLoggedIn: true,
      userStatus:{logged:true,user:[obj]}
    })
   } 

  QSetView = (obj) => {
    this.setState({
      currentPage: obj.page,
    });
  };

  // Function to handle user logout
  handleLogout = () => {
    axios.post("http://88.200.63.148:5015/User/logout")
      .then(() => {
        // Clear the user status in state and redirect to home page
        this.setState({
          isLoggedIn: false,
          userStatus: { logged: false, user: null },
        });
        this.QSetView({ page: "home" });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  QGetView = (state) => {
    let page = state.currentPage;
    switch (page) {
      case "studygroup":
        return <StudyGroup isLoggedIn={this.state.isLoggedIn} userStatus={this.state.userStatus} QIDFromChild={this.QSetView} />;
      case "addmaterial":
        return <AddMaterial QIDFromChild={this.QSetView}/>;
      case "addstudygroup":
        return <AddStudyGroup QIDFromChild={this.QSetView}/>;
      case "signup":
        return <SignupView QUserFromChild={this.QHandleUserLog} QViewFromChild={this.QSetView}/>;
      case "login":
        return <LoginView QUserFromChild={this.QSetUser} QViewFromChild={this.QSetView}/>;
      case "materials":
        return <Materials isLoggedIn={this.state.isLoggedIn} QViewFromChild={this.QSetView}/>;
      case "materialrequests":
        return <MaterialRequests QViewFromChild={this.QSetView} />;
      case "joins":
        return <JoinsView QViewFromChild={this.QSetView}/>
      case "mymaterials":
        return <UsersMaterials QViewFromChild={this.QSetView}/>;
      case "profile":
        return (
          <Profile
            QIDFromChild={this.QSetView}
            QViewFromChild={this.QSetView}
          />
        );
      case "editprofile":
        return <EditProfile QUserFromChild={this.QHandleUserLog} QViewFromChild={this.QSetView}/>;
      default:
        return <HomeView QIDFromChild={this.QSetView} />;
    }
  };

  QHandleUserLog = (obj) => {
    this.QSetView({ 
    page: "home", 
    isLoggedIn: true, // Set to true if login is successful
    userStatus: { logged: true, user: [obj] }, });
  };

  render() {
    const { isLoggedIn } = this.state;
    return (
      <div id="APP" className="container">
        <div id="menu" className="row">
          <nav
            className="navbar navbar-expand-lg navbar-light"
            style={{ backgroundColor: "rgba(204, 241, 252, 0.5)" }}
          >
            <div className="container-fluid">
              <svg
                onClick={isLoggedIn ? () => this.QSetView({ page: "profile" }) : null}
                href="#"
                xmlns="http://www.w3.org/2000/svg"
                width="30"
                height="30"
                fill="currentColor"
                className="bi bi-person-circle"
                viewBox="0 0 16 16"
              >
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                <path
                  fillRule="evenodd"
                  d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
                />
              </svg>
              <a
                onClick={() => this.QSetView({ page: "home" })}
                className="navbar-brand"
                href="#"
                style={{ marginLeft: ".6rem" }}
              >
                Hello Freshman
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                {isLoggedIn ? (  // Conditionally render "Add" dropdown menu if user is logged in
                  <> 
                  <li className="nav-item dropdown">
                    <a
                      className="nav-link dropdown-toggle"
                      href="#"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      Add
                    </a>
                    <ul className="dropdown-menu">
                      <li>
                        <a
                          onClick={() => this.QSetView({ page: "addmaterial" })}
                          className="nav-link"
                          href="#"
                        >
                          Material
                        </a>
                      </li>
                      <li>
                        <a
                          onClick={() => this.QSetView({ page: "addstudygroup" })}
                          className="nav-link"
                          href="#"
                        >
                          Group
                        </a>
                      </li>
                    </ul>
                  </li>

                  <li className="nav-item">
                    <a
                      onClick={() =>
                        this.QSetView({ page: "materialrequests" })
                      }
                      className="nav-link "
                      href="#"
                    >
                      Requests
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      onClick={() =>
                        this.QSetView({ page: "joins" })
                      }
                      className="nav-link "
                      href="#"
                    >
                      My Groups
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      onClick={() =>
                        this.QSetView({ page: "mymaterials" })
                      }
                      className="nav-link "
                      href="#"
                    >
                      My Materials
                    </a>
                  </li>
                  
                
                {/* "Log out" button for logged-in users */}
                <li className="nav-item">
                    <a
                      onClick={this.handleLogout}
                      className="nav-link"
                      href="#"
                    >
                      Log out
                    </a>
                  </li>
                </>
              ) : (
                <>
                  <li className="nav-item">
                    <a
                      onClick={() => this.QSetView({ page: "signup" })}
                      className="nav-link "
                      href="#"
                    >
                      Sign up
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      onClick={() => this.QSetView({ page: "login" })}
                      className="nav-link "
                      href="#"
                    >
                      Login
                    </a>
                  </li>
                  </>
              )}
                </ul>
              </div>
            </div>
          </nav>
        </div>

        <div id="viewer" className="row container">
          {this.QGetView(this.state)}
        </div>
      </div>
    );
  }
}

export default App;