import { Component } from "react";
import {
  MDBCol,
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardText,
  MDBCardBody,
  MDBCardImage,
  MDBTypography,
  MDBIcon
} from "mdb-react-ui-kit";
import "./com.css";
import axios from "axios";

// Function to map faculty numbers to option values
const mapFaculty = (faculty) => {
  switch (faculty) {
    case 1:
      return "UP FHŠ";
    case 2:
      return "UP FM";
    case 3:
      return "UP FAMNIT";
    case 4:
      return "UP FTŠ TURISTICA";
    case 5:
      return "UP FVZ";
    case 6:
      return "UP PEF";
    default:
      return "Select Faculty";
  }
};

// Function to map year_of_studies numbers to option values
const mapYearOfStudies = (yearOfStudies) => {
  switch (yearOfStudies) {
    case 1:
      return "1st-Bachelor";
    case 2:
      return "2nd-Bachelor";
    case 3:
      return "3rd-Bachelor";
    case 4:
      return "1st-Master";
    case 5:
      return "2nd-Master";
    case 6:
      return "1st-PhD";
    case 7:
      return "2nd-PhD";
    case 8:
      return "3rd-PhD";
    default:
      return "Select Year of Studies";
  }
};

class Profile extends Component {
  constructor(props){
    super(props)
    this.state={
      User:{}
    }
  }

  QSetViewInParent = (obj) => {
    this.props.QViewFromChild(obj);
  };

  componentDidMount(){
    axios.get('/User/user')
    .then(response=>{
      console.log(response.data)
      this.setState({
        User:response.data
      })
    })
  }

  render() {
    let data=this.state.User
    
     // Map the faculty and year_of_studies values to their corresponding option values
     const facultyOption = mapFaculty(data.faculty);
     const yearOfStudiesOption = mapYearOfStudies(data.year_of_studies);

    return (
      <section className="vh-100" style={{ backgroundColor: "#f4f5f7" }}>
        <MDBContainer className="py-5 h-100">
          <MDBRow className="justify-content-center align-items-center h-100">
            <MDBCol lg="6" className="mb-4 mb-lg-0">
              <MDBCard className="mb-3" style={{ borderRadius: ".5rem" }}>
                <MDBRow className="g-0">
                  <MDBCol
                    md="4"
                    className="gradient-custom text-center text-white"
                    style={{
                      borderTopLeftRadius: ".5rem",
                      borderBottomLeftRadius: ".5rem"
                    }}
                  >
                    <MDBCardImage
                      src="https://github.com/OlgaKoplik/CodePen/blob/master/profile.jpg?raw=true"
                      alt="Avatar"
                      className="my-5"
                      style={{ width: "80px", borderRadius: "50%" }}
                      fluid
                    />
                    <MDBTypography tag="h5">{data.name} {data.surname}</MDBTypography>
                    <MDBTypography
                      tag="h6"
                      onClick={() =>
                        this.QSetViewInParent({ page: "home", id: 1 })
                      }
                      href="#"
                      id="profileButton"
                      style={{
                        textAlign: "center",
                        margin: "auto",
                        marginTop: "30%",
                        marginBottom: "10%",
                        color: "white",
                        backgroundColor: "rgb(71, 144, 230)",
                        width: "50%",
                        textDecoration: "none"
                      }}
                    >
                      Home
                    </MDBTypography>
                    <MDBTypography
                      tag="h6"
                      onClick={() =>
                        this.QSetViewInParent({ page: "editprofile", id: 1 })
                      }
                      href="#"
                      id="profileButton"
                      style={{
                        textAlign: "center",
                        margin: "auto",
                        marginTop: "5%",
                        marginBottom: "10%",
                        color: "white",
                        backgroundColor: "rgb(71, 144, 230)",
                        width: "50%",
                        textDecoration: "none"
                      }}
                    >
                      Edit
                    </MDBTypography>
                  </MDBCol>
                  <MDBCol md="8">
                    <MDBCardBody className="p-4">
                      <MDBTypography tag="h6">Education</MDBTypography>
                      <hr className="mt-0 mb-4" />
                      <MDBRow className="pt-1">
                        <MDBCol size="6" className="mb-3">
                          <MDBTypography tag="h6">Faculty</MDBTypography>
                          <MDBCardText className="text-muted">
                            {facultyOption}
                          </MDBCardText>
                        </MDBCol>
                        <MDBCol size="6" className="mb-3">
                          <MDBTypography tag="h6">
                            Year of studies
                          </MDBTypography>
                          <MDBCardText className="text-muted">
                            {yearOfStudiesOption}
                          </MDBCardText>
                        </MDBCol>
                      </MDBRow>

                      <MDBTypography tag="h6">Contact</MDBTypography>
                      <hr className="mt-0 mb-4" />
                      <MDBTypography tag="h6">Email</MDBTypography>
                      <MDBCardText className="text-muted">
                        {data.email}
                      </MDBCardText>

                      <div className="d-flex justify-content-start">
                        <a href="#!">
                          <MDBIcon fab icon="facebook me-3" size="lg" />
                        </a>
                        <a href="#!">
                          <MDBIcon fab icon="twitter me-3" size="lg" />
                        </a>
                        <a href="#!">
                          <MDBIcon fab icon="instagram me-3" size="lg" />
                        </a>
                      </div>
                    </MDBCardBody>
                  </MDBCol>
                </MDBRow>
              </MDBCard>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>
    );
  }
}
export default Profile;
