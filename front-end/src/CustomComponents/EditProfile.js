import { Component } from "react";
import axios from "axios";

// Function to map faculty numbers to option values
const mapFaculty = (faculty) => {
    switch (faculty) {
      case 1:
        return "UP FHŠ";
      case 2:
        return "UP FM";
      case 3:
        return "UP FAMNIT";
      case 4:
        return "UP FTŠ TURISTICA";
      case 5:
        return "UP FVZ";
      case 6:
        return "UP PEF";
      default:
        return "Select Faculty";
    }
  };
  
  // Function to map year_of_studies numbers to option values
  const mapYearOfStudies = (yearOfStudies) => {
    switch (yearOfStudies) {
      case 1:
        return "1st-Bachelor";
      case 2:
        return "2nd-Bachelor";
      case 3:
        return "3rd-Bachelor";
      case 4:
        return "1st-Master";
      case 5:
        return "2nd-Master";
      case 6:
        return "1st-PhD";
      case 7:
        return "2nd-PhD";
      case 8:
        return "3rd-PhD";
      default:
        return "Select Year of Studies";
    }
  };

class EditProfile extends Component {
  componentDidMount() {
    axios.get('/User/user')
    .then(response=>{
      console.log(response.data)
      this.setState({
        User:response.data
      })
    })
    

    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#ebf4fa";

  }  

  constructor(props){
    super(props)
    this.state={
      User:{},
      user:[]
    }
  }

  QGetTextFromField=(e)=>{
    this.setState(prevState=>({
        user:{...prevState.user,[e.target.name]:e.target.value}
        }))
    }

    QPostEdit = () => {
      const { email, name, surname, year_of_studies, faculty, password } = this.state.user;
    
      axios.post('/User/edit', {
        email,
        name,
        surname,
        year_of_studies,
        faculty,
        password
      })
        .then(response => {
          this.componentDidMount();
          console.log("Edited...");
        })
        .catch(err => {
          console.log(err);
        });
    };

  QSendUserToParent = (state) => {
    this.props.QUserFromChild(state.user);
  };

  QSetViewInParent = (obj) => {
    this.props.QViewFromChild(obj);
  };

  render() {
    let data=this.state.User
    
     // Map the faculty and year_of_studies values to their corresponding option values
     const facultyOption = mapFaculty(data.faculty);
     const yearOfStudiesOption = mapYearOfStudies(data.year_of_studies);
     return (
        <div
          className="card"
          style={{
            maxWidth: "400px",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "10px"
          }}
        >
          <h3
            style={{ marginLeft: "auto", marginRight: "auto", marginTop: "10px" }}
          >
            Edit Profile
          </h3>
          <form style={{ margin: "20px" }}>
            <div className="mb-3">
              <label className="form-label">Name</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="name"
                type="text"
                className="form-control"
                aria-describedby="emailHelp"
                defaultValue={data.name}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Surname</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="surname"
                type="text"
                className="form-control"
                aria-describedby="emailHelp"
                defaultValue={data.surname}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Year of studies</label>
              <select onChange={(e)=>this.QGetTextFromField(e)} name="year_of_studies" type="text" className="form-select" aria-label="year_of_studies">
                <option selected></option>
                <option value="1">1st-Bachelor</option>
                <option value="2">2nd-Bachelor</option>
                <option value="3">3rd-Bachelor</option>
                <option value="4">1st-Master</option>
                <option value="5">2nd-Master</option>
                <option value="6">1st-PhD</option>
                <option value="7">2nd-PhD</option>
                <option value="8">3rd-PhD</option>
              </select>
            </div>
            <div className="mb-3">
              <label className="form-label">Faculty</label>
              <select onChange={(e)=>this.QGetTextFromField(e)} name="faculty" type="text" className="form-select" aria-label="faculty">
                <option selected></option>
                <option value="1">UP FHŠ</option>
                <option value="2">UP FM</option>
                <option value="3">UP FAMNIT</option>
                <option value="4">UP FTŠ TURISTICA</option>
                <option value="5">UP FVZ</option>
                <option value="6">UP PEF</option>
              </select>
            </div>
            <div className="mb-3">
              <label className="form-label">Email address</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="email"
                type="email"
                className="form-control"
                aria-describedby="emailHelp"
                defaultValue={data.email}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="password"
                className="form-control"
                defaultValue={data.password}
              />
            </div>
          </form>
          <button
            onClick={(e) => {
              this.QPostEdit();
              this.QSendUserToParent(this.state);
              this.QSetViewInParent({ page: "home", id: 1 })
            }}
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              marginBottom: "10px"
            }}
            className="btn btn-primary bt"
          >
            Submit
          </button>
          <button
            onClick={(e) => {
              this.QSetViewInParent({ page: "profile", id: 1 })
            }}
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              marginBottom: "10px"
            }}
            className="btn btn-primary bt"
          >
            Cancel
          </button>
        </div>
      );
  }
}
export default EditProfile;
