import { Component } from "react";
import "./com.css";

class HomeView extends Component {
  componentDidMount() {
    document.body.style.backgroundColor = "#ebf4fa";
    document.body.style.backgroundRepeat = "no-repeat";
    document.body.style.backgroundSize = "100%";
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  render() {
    return (
      <div>
        <div className="card" id="homeCard">
          <div className="card-body" style={{ margin: "10%" }}>
            <h1 className="card-title" style={{ fontWeight: "bold" }}>
              Hello Freshman
            </h1>
            <p className="card-text">
              Welcome to the best study helping webpage!
            </p>
            <button
              id="homeButtons"
              onClick={() =>
                this.QSetViewInParent({ page: "materials", id: 1 })
              }
              href="#"
              className="btn btn-primary"
            >
              <h5>Materials</h5>
            </button>
            <button
              id="homeButtons"
              onClick={() =>
                this.QSetViewInParent({ page: "studygroup", id: 1 })
              }
              href="#"
              className="btn btn-primary"
            >
              <h5>Study Groups</h5>
            </button>
          </div>
          <p>
            <a
              className="btn btn-outline-light"
              data-bs-toggle="collapse"
              href="#collapseExample"
              role="button"
              aria-expanded="false"
              aria-controls="collapseExample"
              style={{
                color: "#072359",
                borderColor: "#072359"
              }}
            >
              Why Hello Freshman
            </a>
          </p>
        </div>

        <div className="collapse" id="collapseExample">
          <div className="card" id="abouttitle">
            <div className="card-body" style={{ textAlign: "center" }}>
              <h3 className="card-title" style={{ fontWeight: "bolder" }}>
                Start sharing and getting knowledge!
              </h3>
            </div>
          </div>
          <div style={{ float: "left" }}>
            <div className="card" id="aboutMaterialsCard">
              <h4 id="aboutCardText">
                Share and get study materials from one place
              </h4>
            </div>
            <h6 className="WhyText">
              <p style={{ backgroundColor: "#916c47", color: "white" }}>
                Do you have dificulties in finding good materials for studying?
              </p>{" "}
              With this platform you can rent or buy materials for studying, for
              which you have dificulties.
            </h6>
            <h6 className="WhyText">
              <p style={{ backgroundColor: "#916c47", color: "white" }}>
                Do you have materials that you do not use anymore?
              </p>{" "}
              Those materials do not have to go to waste. With this platform you
              can help other students with providing them your old materials.
            </h6>
          </div>
          <div style={{ float: "right" }}>
            <div className="card" id="aboutGroupsCard">
              <h4 id="aboutCardText">
                Form your own or join other study groups
              </h4>
            </div>
            <h6 className="WhyText">
              <p style={{ backgroundColor: "#273961", color: "white" }}>
                Do you have dificulties with studying and addapting to a new
                material and place?
              </p>
              With this platform you can join study groups, that will help you
              in your studies. With joining study groups you will not only get
              oportunity for better study process but also make new frendships.
            </h6>
            <h6 className="WhyText">
              <p style={{ backgroundColor: "#273961", color: "white" }}>
                Do you have problem with finding study group that suits your
                needs?
              </p>
              You can also form your own study groups. Form that in such a way
              that is most suitable for you and wait for other students to join
              you.
            </h6>
          </div>
        </div>
      </div>
    );
  }
}
export default HomeView;
