import { Component } from "react";
import axios from "axios";

class SignupView extends Component {
  constructor(props)
  {
      super(props);
      this.state={
          user:{
              type:"signup"
          },
          emailFormatError: false,
      }
  }

  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#ebf4fa";
  }

  QGetTextFromField = (e) => {
    const { name, value } = e.target;

    if (name === "email") {
      // Email format validation using a regular expression
      const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      const isValidEmail = emailPattern.test(value);

      this.setState({
        emailFormatError: !isValidEmail,
        user: { ...this.state.user, [name]: value }
      });
    } else {
      this.setState((prevState) => ({
        user: { ...prevState.user, [name]: value }
      }));
    }
  };

  QSendUserToParent = (state) => {
    this.props.QUserFromChild(state.user);
  };

  QSetViewInParent = (obj) => {
    this.props.QViewFromChild(obj);
  };

  QPostSignup=()=>{

    axios.post('/User/register',{
      email:this.state.user.email,
      name:this.state.user.name,
      surname:this.state.user.surname,
      year_of_studies:this.state.user.year_of_studies,
      faculty:this.state.user.faculty,
      password:this.state.user.password
    })
    .then(response=>{
      console.log("Sent to server...")
    })
    .catch(err=>{
      console.log(err)
    })
  }
  

  render() {
    const { emailFormatError } = this.state;
    return (
      <div
        className="card"
        style={{
          maxWidth: "400px",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "10px"
        }}
      >
        <h3
          style={{ marginLeft: "auto", marginRight: "auto", marginTop: "10px" }}
        >
          Create Account
        </h3>
        <form style={{ margin: "20px" }}>
          <div className="mb-3">
            <label className="form-label">Name</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="name"
              type="text"
              className="form-control"
              aria-describedby="emailHelp"
              required
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Surname</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="surname"
              type="text"
              className="form-control"
              aria-describedby="emailHelp"
              required
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Year of studies</label>
            <select onChange={(e)=>this.QGetTextFromField(e)} name="year_of_studies" type="text" className="form-select" aria-label="year_of_studies">
              <option selected></option>
              <option value="1">1st-Bachelor</option>
              <option value="2">2nd-Bachelor</option>
              <option value="3">3rd-Bachelor</option>
              <option value="4">1st-Master</option>
              <option value="5">2nd-Master</option>
              <option value="6">1st-PhD</option>
              <option value="7">2nd-PhD</option>
              <option value="8">3rd-PhD</option>
            </select>
          </div>
          <div className="mb-3">
            <label className="form-label">Faculty</label>
            <select onChange={(e)=>this.QGetTextFromField(e)} name="faculty" type="text" className="form-select" aria-label="faculty">
              <option selected></option>
              <option value="1">UP FHŠ</option>
              <option value="2">UP FM</option>
              <option value="3">UP FAMNIT</option>
              <option value="4">UP FTŠ TURISTICA</option>
              <option value="5">UP FVZ</option>
              <option value="6">UP PEF</option>
            </select>
          </div>
          <div className="mb-3">
            <label className="form-label">Email address</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="email"
              type="email"
              className="form-control"
              aria-describedby="emailHelp"
              required
            />
            {this.state.emailFormatError && (
          <div className="alert alert-danger" role="alert">
            Please enter a valid email address.
          </div>
        )}
            <div id="emailHelp" className="form-text">
              We'll never share your email with anyone else.
            </div>
          </div>
          <div className="mb-3">
            <label className="form-label">Password</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="password"
              type="password"
              className="form-control"
              required
            />
          </div>
        </form>
        <button
          onClick={(e) => {
            e.preventDefault();
            this.QPostSignup();
            this.QSendUserToParent(this.state);
            this.QSetViewInParent({ page: "login", id: 1 })
          }}
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            marginBottom: "10px"
          }}
          className="btn btn-primary bt"
          disabled={emailFormatError}
        >
          Submit
        </button>
      </div>
    );
  }
}
export default SignupView;