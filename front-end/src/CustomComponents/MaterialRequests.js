import { Component } from "react";
import "./com.css";
import axios from "axios";

class MaterialRequests extends Component {
  constructor(props){
    super(props)
    this.state={
      Requests:[]
    }
  }

  QSetViewInParent = (obj) => {
    this.props.QViewFromChild(obj);
  };

  componentDidMount(){
    axios.get('/MatRequest/user-requests')
    .then(response=>{
      console.log(response.data)
      this.setState({
        Requests:response.data
      })
    })
  };

  acceptRequest = (id) => {
    axios.post('/MatRequest/accept-request', { id })
      .then(response => {
        console.log(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  };
  
  

  render() {
    let data=this.state.Requests
    return (
      <div>
        <div style={{ marginBottom: "20px" }}>
          <a
            onClick={() => this.QSetViewInParent({ page: "home", id: 1 })}
            href="#"
            className="homebutton"
            style={{
              float: "left",
              color: "rgb(78, 147, 245)",
              textDecoration: "none"
            }}
          >
            <h6>Home</h6>
          </a>
          <a
            onClick={() => this.QSetViewInParent({ page: "materials", id: 1 })}
            href="#"
            className="homebutton"
            style={{
              float: "right",
              color: "rgb(78, 147, 245)",
              textDecoration: "none"
            }}
          >
            <h6>Materials</h6>
          </a>
        </div>
        <div
          className="card-body"
          style={{
            textAlign: "center",
            margin: "2%"
          }}
        >
          <h3 className="card-title">Rent Requests</h3>
        </div>
        <div className="row row-cols-1 row-cols-md-3 g-4">
          { data.length > 0 ?
            data.reverse().map ( (d)=> {
              return(
          <div className="col-sm-6" key={d.id}>
            <div className="card">
              <div className="card-body">
                <h4 className="card-header" style={{ fontWeight: "bold" }} >
                  {d.title}
                </h4>
                <p className="card-text">{d.m_name}</p>
                <p>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-book"
                    viewBox="0 0 16 16"
                  >
                    <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                  </svg>{" "}
                  <b>Type:</b> {d.type}
                </p>
                <p>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-pen"
                    viewBox="0 0 16 16"
                  >
                    <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z" />
                  </svg>{" "}
                  <b>Subject:</b> {d.subject}
                </p>
                <p>
                  <b>From:</b> {d.name} {d.surname}
                  <br />
                  <b>E-mail:</b> {d.email}
                </p>
                <a href={`mailto:${d.email}`} className="btn btn-primary"
                onClick={() => this.acceptRequest(d.id)}>
                  Accept
                </a>
              </div>
            </div>
          </div>
          )
        })
        : ""}
        </div>
      </div>
    );
  }
}
export default MaterialRequests;
