import { Component } from "react";
import axios from "axios";

class AddMaterial extends Component {
  constructor(props){
    super(props)
    this.state={
      material:[]
    }
  }

  QGetTextFromField=(e)=>{
    this.setState(prevState=>({
        material:{...prevState.material,[e.target.name]:e.target.value}
        }))
    }

  QPostNovica=()=>{
      axios.post('/Material',{
        title:this.state.material.title,
        m_name:this.state.material.m_name,
        type:this.state.material.type,
        subject:this.state.material.subject
      },
      {
        withCredentials: true, // Send cookies with the request
      })
      .then(response=>{
        console.log("Sent to server...")
      })
      .catch(err=>{
        console.log(err)
      })
  }
    
  QSetViewInParent = (obj) => {
      this.props.QIDFromChild(obj);
  };
  
  render() {
    return (
      <div
        className="card"
        style={{
          maxWidth: "400px",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "30px"
        }}
      >
        <h3 style={{ margin: "10px", textAlign: "center" }}>Share materials</h3>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Title</label>
          <input name="title" onChange={(e)=>this.QGetTextFromField(e)} type="text" className="form-control" placeholder="Title..." />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Name</label>
          <input name="m_name" onChange={(e)=>this.QGetTextFromField(e)} type="text" className="form-control" placeholder="Name..." />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Subject</label>
          <input
            name="subject"
            onChange={(e)=>this.QGetTextFromField(e)}
            type="text"
            className="form-control"
            placeholder="Subject..."
          />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Type</label>
          <input name="type" onChange={(e)=>this.QGetTextFromField(e)} type="text" className="form-control" placeholder="Type..." />
        </div>
        <button
          onClick={()=>{this.QPostNovica(); this.QSetViewInParent({ page: "materials", id: 1 })}}
          className="btn btn-primary bt"
          style={{ margin: "auto", marginBottom: "10px" }}
        >
          Upload
        </button>
      </div>
    );
  }
}
export default AddMaterial;
