import { Component } from "react";
import "./com.css";
import axios from "axios";

class StudyGroup extends Component {
  constructor(props){
    super(props)
    this.state={
      Group:[],
      groupMembers: {}
    }
  }


  componentDidMount(){
    axios.get('/StudyGroup')
    .then(response=>{
      console.log(response.data)
      this.setState({
        Group:response.data
      })
    });

    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#ebf4fa";
    this.fetchStudyGroups();
  }

  // Function to fetch study groups
  fetchStudyGroups() {
    axios.get('/StudyGroup')
      .then(response => {
        this.setState({ Group: response.data });
        // After fetching study groups, fetch group members for each group
        response.data.forEach(group => {
          this.fetchGroupMembers(group.id);
        });
      })
      .catch(error => {
        console.error("Error fetching study groups:", error);
      });
  }

  // Function to fetch group members for a specific group
  fetchGroupMembers(groupId) {
    axios.get(`/StudyGroup/${groupId}/members`)
      .then(response => {
        const members = response.data;
        const memberNames = members.map(member => `${member.name} ${member.surname}`);
        // Store the member names for the specific group in state
        this.setState(prevState => ({
          groupMembers: {
            ...prevState.groupMembers,
            [groupId]: memberNames
          }
        }));
      })
      .catch(error => {
        console.error(`Error fetching members for group ID ${groupId}:`, error);
      });
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };


  // Helper function to format time (hours:minutes)
  formatTime = (timeString) => {
    const [hours, minutes] = timeString.split(":");
    const formattedTime = `${hours.padStart(2, "0")}:${minutes.padStart(2, "0")}`;
    return formattedTime;
  };

  // Helper function to format date (day.month.year)
formatDate = (dateString) => {
  const date = new Date(dateString); // Parse the ISO 8601 date string
  const day = date.getUTCDate().toString().padStart(2, "0");
  const month = (date.getUTCMonth() + 1).toString().padStart(2, "0"); // Months are zero-based
  const year = date.getUTCFullYear();
  const formattedDate = `${day}.${month}.${year}`;
  return formattedDate;
};

// Add a function to handle the join action
handleJoin = (groupId) => {
  // Get the logged-in user's email (ID) from the session
  const userEmail = this.props.isLoggedIn ? this.props.userStatus.user[0].email : null;
  
  if (!userEmail) {
    return;
  }

  // Send a request to the backend to join the group
  axios.post(`/StudyGroup/${groupId}/join`, { userEmail }, {withCredentials: true})
    .then(response => {
      // Refresh the study group list after successful join
      this.componentDidMount();
    })
    .catch(error => {
      console.log(error);
    });
};

// Function to check if the "Join" button should be disabled
isButtonDisabled = (numOfPeople) => {
  return numOfPeople === 0;
};


  render() {
    let data=this.state.Group
    const { isLoggedIn } = this.props;

    return (
      <div>
        <div>
          <a
            onClick={() => this.QSetViewInParent({ page: "home", id: 1 })}
            href="#"
            className="homebutton"
            style={{
              float: "left",
              color: "rgb(78, 147, 245)",
              textDecoration: "none"
            }}
          >
            <h6>Home</h6>
          </a>
          <a
            onClick={() => this.QSetViewInParent({ page: "materials", id: 1 })}
            href="#"
            className="homebutton"
            style={{
              float: "right",
              color: "rgb(78, 147, 245)",
              textDecoration: "none"
            }}
          >
            <h6>Materials</h6>
          </a>
        </div>
        <div
          className="card-body"
          style={{
            textAlign: "center",
            margin: "2%"
          }}
        >
          <h3 className="card-title">Study Groups</h3>
        </div>
        <div
          className="row row-cols-1 row-cols-md-3 g-4"
          style={{ margin: "10px" }}
        >

        {data.length>0 ?
          data.reverse().map ( (d)=> {
            const formattedTime = this.formatTime(d.time);
        const formattedDate = this.formatDate(d.date);
        const memberNames = this.state.groupMembers[d.id] || [];
            return(
              <div className="col" key={d.id}>
            <div className="card">
              <div className="card-body">
                  <svg
                    href="#"
                    xmlns="http://www.w3.org/2000/svg"
                    width="30"
                    height="30"
                    fill="currentColor"
                    className="bi bi-person-circle"
                    viewBox="0 0 16 16"
                    style={{ float: "left", marginRight: "5px" }}
                  >
                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                    <path
                      fillRule="evenodd"
                      d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
                    />
                  </svg>
                <h5
                  className="card-title"
                  style={{ float: "left", margin: "5px" }}
                >
                  {d.name} {d.surname}
                </h5>
              </div>
              <div style={{ textAlign: "center" }}>
                <h5 style={{ color: "blue" }}>{d.topic}</h5>
                <p>
                  Number of people: <b>{d.num_of_people}</b>
                </p>
                <div>
                  <div style={{ display: "inline-block", margin: "15px" }}>
                    <b>Time:</b> <p>{formattedTime}</p>
                  </div>
                  <div style={{ display: "inline-block", margin: "15px" }}>
                    <b>Date:</b> <p>{formattedDate}</p>
                  </div>
                  <div style={{ display: "inline-block", margin: "15px" }}>
                    <b>Location:</b> <p>{d.location}</p>
                  </div>
                </div>
              </div>
              <div style={{textAlign:"center"}}>
              <h6>Participants:</h6>
               {/* Display member names here */}
               {memberNames.map((name, index) => (
                <div style={{textAlign:"center"}}>
                      <svg
                      href="#"
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      className="bi bi-person-circle"
                      viewBox="0 0 16 16"
                      style={{display:"inline", marginRight:"5"}}
                    >
                      <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                      <path
                        fillRule="evenodd"
                        d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
                      />
                    </svg>
                      <p style={{margin:"0", display:"inline"}} key={index}>{name}</p>
                      <br/>
                </div>
                    ))}
              </div>
              <div style={{ textAlign: "center", marginBottom: "10px", marginTop:"15px" }}>
              {isLoggedIn && (
                    <>
                        <button
                  style={{ margin: "auto", marginBottom: "10px" }}
                  className="btn btn-primary bt"
                  onClick={() => this.handleJoin(d.id)}
                  disabled={this.isButtonDisabled(d.num_of_people)}
                >
                  Join
                </button>
                    </>
                  )}
              </div>
            </div>
          </div>
            )
          }) : ""
        }

        </div>
      </div>
    );
  }
}
export default StudyGroup;
