import { Component } from "react";
import axios from "axios";

class LoginView extends Component {
  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#ebf4fa";

  }

  constructor(props) {
    super(props);
    this.state = {
      user: {
        type: "login"
      },
      errorMessage: "",
    };
  }

  QPostLogin=()=>{
    axios.post('/User/login',{
      email:this.state.user.email,
      password:this.state.user.password
    },{withCredentials:true})
    .then(response=>{
      console.log("Sent to server...")
      console.log(response.data[0])
      this.QSendUser2Parent(response.data[0])
      // Only call QSendUserToParent if login is successful
      this.QSendUserToParent(this.state);
    })
    .catch(err=>{
      console.log(err);
      // Update the errorMessage state with the error message from the server response
      if (err.response && err.response.data) {
        this.setState({ errorMessage: err.response.data.error });
      } else {
        this.setState({ errorMessage: "An error occurred." });
      }
      
    })
  }

  QSendUser2Parent=(obj)=>{
    this.props.QUserFromChild(obj)
  }   


  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      user: { ...prevState.user, [e.target.name]: e.target.value }
    }));
  };

  QSendUserToParent = (state) => {
    this.props.QViewFromChild(state.user);
  };

  QSetViewInParent = (obj) => {
    this.props.QViewFromChild(obj);
  };

  render() {
    return (
      <div
        className="card"
        style={{
          maxWidth: "400px",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "30px"
        }}
      >
        <h3
          style={{ marginLeft: "auto", marginRight: "auto", marginTop: "10px" }}
        >
          Login
        </h3>
        <form style={{ margin: "20px" }}>
          <div className="mb-3">
            <label className="form-label">Email address</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="email"
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Password</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="password"
              type="password"
              className="form-control"
              id="exampleInputPassword1"
            />
            {this.state.errorMessage && (
              <div className="text-danger">{this.state.errorMessage}</div>
            )}
          </div>
        </form>
        <p><a onClick={() => this.QSetViewInParent({ page: "signup", id: 1 })} className="link-opacity-75-hover" href="#">Don't have an account?</a></p>
        <button
          onClick={() => {
            this.QPostLogin();
          }}
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            marginBottom: "10px"
          }}
          className="btn btn-primary bt"
        >
          Login
        </button>
      </div>
    );
  }
}
export default LoginView;
