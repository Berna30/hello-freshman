import { Component } from "react";
import axios from "axios";

class AddStudyGroup extends Component {
  constructor(props){
    super(props)
    this.state={
      studygroup:[]
    }
  }

  QGetTextFromField=(e)=>{
    this.setState(prevState=>({
        studygroup:{...prevState.studygroup,[e.target.name]:e.target.value}
        }))
    }

    QPostNovica=()=>{
      axios.post('/StudyGroup',{
        topic:this.state.studygroup.topic,
        num_of_people:this.state.studygroup.num_of_people,
        time:this.state.studygroup.time,
        date:this.state.studygroup.date,
        location:this.state.studygroup.location
      },
      {
        withCredentials: true, // Send cookies with the request
      })
      .then(response=>{
        console.log("Sent to server...")
      })
      .catch(err=>{
        console.log(err)
      })
    }

    QSetViewInParent = (obj) => {
      this.props.QIDFromChild(obj);
    };

  render() {
    return (
      <div
        className="card"
        style={{
          maxWidth: "400px",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "30px"
        }}
      >
        <h3 style={{ margin: "10px", textAlign: "center" }}>From a group</h3>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Topic</label>
          <input
            name="topic"
            onChange={(e)=>this.QGetTextFromField(e)}
            type="text"
            className="form-control"
            placeholder="Topic..."
            required
          />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Number of people</label>
          <input
            name="num_of_people"
            onChange={(e)=>this.QGetTextFromField(e)}
            type="number"
            pattern="[0-9]*"
            className="form-control"
            placeholder="0"
            required
          />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Time</label>
          <input name="time" onChange={(e)=>this.QGetTextFromField(e)} type="time" min="09:00" max="18:00" required />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Date</label>
          <input name="date" onChange={(e)=>this.QGetTextFromField(e)} type="date" required />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Location</label>
          <input
            name="location"
            onChange={(e)=>this.QGetTextFromField(e)}
            type="text"
            className="form-control"
            placeholder="Location..."
            required
          />
        </div>
        <button
          onClick={()=>{this.QPostNovica(); this.QSetViewInParent({ page: "studygroup", id: 1 })}}
          className="btn btn-primary bt"
          style={{ margin: "auto", marginBottom: "10px" }}
        >
          Upload
        </button>
      </div>
    );
  }
}
export default AddStudyGroup;
