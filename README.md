# Hello Freshman

Hello Freshman is an information system for study material and knowledge sharing.

### Table of contents
- General information
- Technologies used
- Setup

## General Information

The aim of this information system is to give opportunity for students to borrow,
rent or buy materials, but also on the other side be able to give those materials.
Also connect individuals for making groups that will help each other for the study purposes.

The app is developed in such a way that it is easy for the user to list through the
already posted materials and study groups. Renting request and joining a group is 
only a button click away from the user. While the materials and study groups are
available for everyone to see, only the user that has an account have access to 
rent or post materials and join or form study groups.

## Technologies used
 - [Node.js](https://nodejs.org/en) + [Express.js](https://expressjs.com/)
 - [React.js](https://react.dev/)
 - MySQL + [phpMyAdmin](http://88.200.63.148/phpmyadmin/)

## Setup

To launch the app the user should use this link:

[http://88.200.63.148:3015](http://88.200.63.148:5015)