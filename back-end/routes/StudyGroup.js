const express = require("express")
const StudyGroup = express.Router();
const DB = require('../db/conn.js')

//Get study groups
StudyGroup.get('/', async (req, res, next)=>{
    try {
      let userEmail = null;

      if(req.session && req.session.user){
        userEmail = req.session.user[0].email;
      }

      var queryResult;
      if(userEmail){
        // If the user is logged in, get study groups excluding their own
        queryResult = await DB.allGroupsExcludingUser(userEmail);
      }
      else{
        // If no user is logged in, get all study groups
        queryResult = await DB.allGroups();
      }
        res.json(queryResult)
    } catch (err) {
        console.log(err)
        res.sendStatus(500)
    }
})

//Post new study group
StudyGroup.post('/', async (req, res, next)=>{
    let topic = req.body.topic
    let num_of_people = req.body.num_of_people
    let time = req.body.time
    let date = req.body.date
    let location = req.body.location

    // Check if the user is logged in
  if (!req.session || !req.session.user) {
    return res.status(401).json({ error: "User not logged in" });
  }

  let email = req.session.user[0].email; // Get the user's email

    var isAcompleteGroup = topic && num_of_people && time && date && location
    if(isAcompleteGroup){
        try {
            var queryResult = await DB.creteGroup(topic,num_of_people,time,date,location, email)
            if(queryResult.affectedRows){
                console.log("New group added!")
            }
        } catch (err) {
            console.log(err)
            res.sendStatus(500)
        }
    }
    else{
        console.log("A field is empty!")
    }
    res.end();
})

//Post new study group join
StudyGroup.post('/:id/join', async (req, res, next) => {
    const groupId = req.params.id;
    const userEmail = req.body.userEmail;
  
    try {
      // Check if the user has already joined this group
      const checkJoined = await DB.checkUserJoinedGroup(groupId, userEmail);
      if (checkJoined.length > 0) {
        return res.status(400).json({ error: "User already joined this group" });
      }
  
      // Update the num_of_people in the Study Group table
      await DB.decreaseNumOfPeople(groupId);
  
      // Save the join information in the User_join_StudyGroup table
      await DB.saveUserJoinGroup(groupId, userEmail);
  
      res.json({ message: "Successfully joined the study group" });
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });

// Route to get group members for a specific group
StudyGroup.get("/:id/members", async (req, res) => {
    const groupId = req.params.id;
    try {
      const members = await DB.getGroupMembers(groupId);
      res.json(members);
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });


module.exports=StudyGroup