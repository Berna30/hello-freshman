const express = require("express")
const User = express.Router();
const DB = require('../db/conn.js')

//Session for login
User.get('/login',(req,res)=>{
    if(req.session.user) 
    {
    res.send({
         logged:true,
         user:req.session.user
     })
  
    }
    else
    {
        res.send({logged:false})
    }
  })
  

//User login
User.post('/login', async (req, res)=>{
    var email = req.body.email;
    var password = req.body.password;

    if(email && password){
        try {
            let queryResult = await DB.AuthUser(email);

                if(queryResult.length>0){
                    if(password===queryResult[0].password){
                        req.session.user=queryResult;
                        console.log("Session valid");
                        res.json(queryResult);
                    }
                    else{
                        console.log("Incorrect password");
                        res.status(400).json({ error: "Incorrect password" });
                    }
                }
                else{
                    console.log("User not registred");
                    res.status(400).json({ error: "User not registered" });
                }
        } catch (err) {
            console.log(err)
            res.sendStatus(500)
        }
    }
    else{
        console.log("Please enter Email and Password");
        res.status(400).json({ error: "Please enter Email and Password" });
    }
    res.end();
})

//Edit user
User.post('/edit', async (req, res) => {
    const updatedFields = {
      email: req.body.email,
      name: req.body.name,
      surname: req.body.surname,
      year_of_studies: req.body.year_of_studies,
      faculty: req.body.faculty,
      password: req.body.password
    };
    const userEmail = req.session.user[0].email;
  
    try {
      Object.keys(updatedFields).forEach(key => {
        if (updatedFields[key] === undefined) {
          delete updatedFields[key];
        }
      });
  
      if (Object.keys(updatedFields).length > 0) {
        let queryResult = await DB.EditUser(
          updatedFields.email,
          updatedFields.name,
          updatedFields.surname,
          updatedFields.year_of_studies,
          updatedFields.faculty,
          updatedFields.password,
          userEmail
        );
  
        if (queryResult.affectedRows) {
          if (updatedFields.email) {
            req.session.user[0].email = updatedFields.email;
          }
          console.log("User edited!");
        }
      } else {
        console.log("No fields to update.");
      }
      res.end();
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });
  
//Add new user
User.post('/register', async(req,res)=>{
  let email = req.body.email
  let name = req.body.name
  let surname = req.body.surname
  let year_of_studies = req.body.year_of_studies
  let faculty = req.body.faculty
  let password = req.body.password

  var isAcompleteUser = email && name && surname && year_of_studies && faculty && password
  if(isAcompleteUser){
      try {
          let queryResult=await DB.AddUser(email,name,surname,year_of_studies,faculty,password);
          if(queryResult.affectedRows){
              console.log("New user added!")
          }
      } catch (err) {
          console.log(err)
          res.sendStatus(500)
      }
  }
  else{
      console.log("A field is missing!")
  }
  res.end();
})

// New route to fetch logged-in user data
User.get('/user', async (req, res) => {
    const loggedInUserId = req.session.user[0].email; // Get the logged-in user's ID (email) from the session
  
    if (!loggedInUserId) {
      return res.status(400).send("Logged-in user ID not available.");
    }
  
    try {
      const user = await DB.getUserById(loggedInUserId);
  
      if (user.length > 0) {
        res.send(user[0]);
      } else {
        res.status(404).send("User not found.");
      }
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });

//User logout
User.post('/logout', (req, res) => {
    req.session.destroy((err) => {
      if (err) {
        console.log(err);
        return res.sendStatus(500);
      }
      console.log('User logged out');
      return res.sendStatus(200);
    });
  });

module.exports=User