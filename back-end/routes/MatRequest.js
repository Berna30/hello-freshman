const express = require("express")
const MatRequest = express.Router();
const DB = require('../db/conn.js')


//Get the material requests of the logged in user
MatRequest.get('/user-requests', async (req, res, next) => {
    if (!req.session.user) {
      return res.status(401).json({ error: "User not logged in." });
    }
  
    try {
      const loggedInUserEmail = req.session.user[0].email;
      const userRequests = await DB.getMaterialRequestsForUser(loggedInUserEmail);
      res.json(userRequests);
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });
  

//Post new material request
MatRequest.post('/', async (req, res, next)=>{
    let m_id = req.body.m_id;

    // Check if the user is logged in
  if (!req.session || !req.session.user) {
    return res.status(401).json({ error: "User not logged in" });
  }

  let email = req.session.user[0].email; // Get the user's email

    if (m_id)
    {
        try {
            var queryResult = await DB.sendRequest(m_id, email)
            if(queryResult.affectedRows){
                console.log("New request sent!")
            }
        } catch (err) {
            console.log(err)
            res.sendStatus(500)
        }
    }
    else{
        console.log("A field is empty!")
    }
    res.end();
})


//Accept a request
MatRequest.post('/accept-request', async (req, res, next) => {
  let id = req.body.id;

  // Check if the user is logged in
  if (!req.session || !req.session.user) {
    return res.status(401).json({ error: "User not logged in" });
  }

  if (id) {
    try {
      // Update the accepted field in User_request_Material
      await DB.acceptRequest(id);

      console.log("Request accepted!");
      res.json({ message: "Request accepted" });
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  } else {
    console.log("A field is empty!");
    res.sendStatus(400);
  }
});


module.exports=MatRequest