const express= require("express")
const Material = express.Router();
const DB = require('../db/conn.js')

//Get materials
Material.get('/', async (req, res, next) => {
    try {
      let userEmail = null;
  
      // Check if the user is logged in
      if (req.session && req.session.user) {
        userEmail = req.session.user[0].email; // Get the user's email
      }
  
      var queryResult;
      if (userEmail) {
        // If the user is logged in, get materials excluding their own materials
        queryResult = await DB.allMaterialsExcludingUser(userEmail);
      } else {
        // If no user is logged in, get all materials
        queryResult = await DB.allMaterials();
      }
  
      res.json(queryResult);
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });
  
  
//Post new material
Material.post('/', async (req, res, next)=>{
    let title = req.body.title
    let m_name = req.body.m_name
    let type = req.body.type
    let subject = req.body.subject 

    // Check if the user is logged in
  if (!req.session || !req.session.user) {
    return res.status(401).json({ error: "User not logged in" });
  }

  let email = req.session.user[0].email; // Get the user's email

    var isAcompleteMaterial = title && m_name && type && subject
    if (isAcompleteMaterial)
    {
        try {
            var queryResult = await DB.creteMaterial(title,m_name,type,subject, email)
            if(queryResult.affectedRows){
                console.log("New material added!")
            }
        } catch (err) {
            console.log(err)
            res.sendStatus(500)
        }
    }
    else{
        console.log("A field is empty!")
    }
    res.end();
})

//Get the materials that have been posted by the logged in user
Material.get('/user-materials', async (req, res, next) => {
  if (!req.session.user) {
    return res.status(401).json({ error: "User not logged in." });
  }

  try {
    const loggedInUserEmail = req.session.user[0].email;
    const userRequests = await DB.getUsersMaterials(loggedInUserEmail);
    res.json(userRequests);
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
});
    
module.exports=Material
