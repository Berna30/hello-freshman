const express = require("express")
const StudyGroupJoin = express.Router();
const DB = require('../db/conn.js')


//Get study groups that have been joined for the logged in user
StudyGroupJoin.get('/user-joins', async (req, res, next) => {
    if (!req.session.user) {
      return res.status(401).json({ error: "User not logged in." });
    }
  
    try {
      const loggedInUserEmail = req.session.user[0].email;
      const userRequests = await DB.getJoinsForUser(loggedInUserEmail);
      res.json(userRequests);
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
});

module.exports=StudyGroupJoin