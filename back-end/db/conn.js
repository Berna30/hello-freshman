const mysql = require("mysql2")

const  conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: process.env.DB_DATABASE,
  })


  let dataPool={}

  //Function to get all materials
  dataPool.allMaterials=()=>{
    return new Promise ((resolve, reject)=>{
      conn.query('SELECT Material.*, User.name, User.surname FROM Material INNER JOIN User ON Material.u_id = User.email', (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }
  
  //Function to get all materials except those of the logged in user
  dataPool.allMaterialsExcludingUser = (userEmail) => {
    return new Promise((resolve, reject) => {
      conn.query(
        'SELECT Material.*, User.name, User.surname FROM Material INNER JOIN User ON Material.u_id = User.email WHERE Material.u_id <> ?',
        [userEmail],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };  

  //Function to get all study groups except those of the logged in user
  dataPool.allGroupsExcludingUser=(userEmail)=>{
    return new Promise ((resolve, reject)=>{
      conn.query('SELECT `Study group`.*, User.name, User.surname FROM `Study group` INNER JOIN User ON `Study group`.u_id = User.email WHERE `Study group`.u_id <> ?', 
      [userEmail],
      (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  //Function to get all groups
  dataPool.allGroups=()=>{
    return new Promise ((resolve, reject)=>{
      conn.query('SELECT `Study group`.*, User.name, User.surname FROM `Study group` INNER JOIN User ON `Study group`.u_id = User.email ', (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  //Function to get requests for the logged in user
  dataPool.getMaterialRequestsForUser = (userEmail) => {
    return new Promise((resolve, reject) => {
      conn.query(
        `SELECT User_request_Material.id, User_request_Material.acepted, Material.title, Material.m_name, Material.type, Material.subject, User.name, User.surname, User.email 
        FROM User_request_Material 
        JOIN Material ON User_request_Material.m_id = Material.id 
        JOIN User ON User_request_Material.u_id = User.email 
        WHERE User_request_Material.acepted=0 AND User_request_Material.m_id IN 
        (SELECT id FROM Material WHERE u_id = ?)`,
        [userEmail],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };

  //Function to get study groups for the logged in user
  dataPool.getJoinsForUser = (userEmail) => {
    return new Promise((resolve, reject) => {
      conn.query(
        'SELECT `Study group`.*, User.name, User.surname FROM `Study group` INNER JOIN User ON `Study group`.u_id = User.email WHERE `Study group`.u_id = ?',
        [userEmail],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };

  //Function to get materials for the logged in user
  dataPool.getUsersMaterials=(userEmail)=>{
    return new Promise ((resolve, reject)=>{
      conn.query('SELECT Material.*, User.name, User.surname FROM Material INNER JOIN User ON Material.u_id = User.email WHERE Material.u_id = ?', 
      [userEmail],
      (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }
  
  //Function to add a new material
  dataPool.creteMaterial = (title, m_name, type, subject, u_id) => {
    return new Promise((resolve, reject) => {
      conn.query('INSERT INTO Material (title, m_name, type, subject, u_id) VALUES (?, ?, ?, ?, ?)',
        [title, m_name, type, subject, u_id],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        });
    });
  };
  
  //Function to add a new group
  dataPool.creteGroup=(topic,num_of_people,time,date,location, u_id)=>{
    return new Promise ((resolve, reject)=>{
      conn.query('INSERT INTO `Study group` (topic,num_of_people,time,date,location, u_id) VALUES (?,?,?,?,?, ?)', [topic, num_of_people, time, date, location, u_id], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  };

  //Function to send material renting request
  dataPool.sendRequest = (m_id, u_id) => {
    return new Promise((resolve, reject) => {
      conn.query(
        'SELECT COUNT(*) AS count FROM User_request_Material WHERE m_id = ? AND u_id = ?',
        [m_id, u_id],
        (selectErr, selectRes) => {
          if (selectErr) {
            return reject(selectErr);
          }
          if (selectRes[0].count > 0) {
            // Combination already exists, do not insert
            return resolve({ message: 'Combination already exists' });
          } else {
            // Combination doesn't exist, insert new record
            conn.query(
              'INSERT INTO User_request_Material (m_id, u_id) VALUES (?, ?)',
              [m_id, u_id],
              (insertErr, insertRes) => {
                if (insertErr) {
                  return reject(insertErr);
                }
                return resolve(insertRes);
              }
            );
          }
        }
      );
    });
  };

  //Function to accept material rent request
  dataPool.acceptRequest = (id) => {
    return new Promise((resolve, reject) => {
      conn.query(
        'UPDATE User_request_Material SET acepted = 1 WHERE id = ?',
        [id],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };
  
  
  //Function to check if a user has already joined a group
  dataPool.checkUserJoinedGroup = (groupId, userEmail) => {
    return new Promise((resolve, reject) => {
      conn.query(
        "SELECT * FROM User_join_StudyGroup WHERE sg_id = ? AND u_id = ?",
        [groupId, userEmail],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };

  //Function to decrease the number of people in the Study Group table
  dataPool.decreaseNumOfPeople = (groupId) => {
    return new Promise((resolve, reject) => {
      conn.query(
        "UPDATE `Study group` SET num_of_people = num_of_people - 1 WHERE id = ?",
        [groupId],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };


  //Function to save the user join information in the User_join_StudyGroup table
  dataPool.saveUserJoinGroup = (groupId, userEmail) => {
    return new Promise((resolve, reject) => {
      conn.query(
        "INSERT INTO User_join_StudyGroup (sg_id, u_id) VALUES (?, ?)",
        [groupId, userEmail],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };

  //Function to get study group members
  dataPool.getGroupMembers = (groupId) => {
    return new Promise((resolve, reject) => {
      conn.query(
        "SELECT User.name, User.surname, User.email FROM User JOIN User_join_StudyGroup ON User.email = User_join_StudyGroup.u_id WHERE User_join_StudyGroup.sg_id = ?",
        [groupId],
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        }
      );
    });
  };

  //Function to authenticate user
  dataPool.AuthUser=(email)=>
  {
    return new Promise ((resolve, reject)=>{
      conn.query('SELECT * FROM User WHERE email = ?', email, (err,res, fields)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })  
    
  }

  //Function to add new user
  dataPool.AddUser=(email,name,surname,year_of_studies,faculty,password)=>{
    return new Promise ((resolve, reject)=>{
      conn.query('INSERT INTO User (email,name,surname,year_of_studies,faculty,password) VALUES (?,?,?,?,?,?)', [email,name,surname,year_of_studies,faculty,password], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  //Function to edit user information
  dataPool.EditUser = (email, name, surname, year_of_studies, faculty, password, userEmail) => {
    return new Promise((resolve, reject) => {
      const updateValues = [];
      const placeholders = [];

      if (email) {
        updateValues.push(email);
        placeholders.push('email=?');
      }
      if (name) {
        updateValues.push(name);
        placeholders.push('name=?');
      }
      if (surname) {
        updateValues.push(surname);
        placeholders.push('surname=?');
      }
      if (year_of_studies) {
        updateValues.push(year_of_studies);
        placeholders.push('year_of_studies=?');
      }
      if (faculty) {
        updateValues.push(faculty);
        placeholders.push('faculty=?');
      }
      if (password) {
        updateValues.push(password);
        placeholders.push('password=?');
      }

      const updateQuery = placeholders.join(', ');

      updateValues.push(userEmail);

      const query = 'UPDATE User SET ' + updateQuery + ' WHERE email=?';

      conn.query(query, updateValues, (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      });
    });
  };

  //Function to get the user by the id provided
  dataPool.getUserById = (id) => {
    return new Promise((resolve, reject) => {
      conn.query('SELECT * FROM User WHERE email = ?', id, (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      });
    });
  };


 conn.connect((err) => {
      if(err){
          console.log("ERROR: " + err.message);
          return;    
      }
      console.log('Connection established');
})
  
module.exports = dataPool
