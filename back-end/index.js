const express = require('express')
const dotenv = require("dotenv")
const app = express()
const cors = require("cors")
const session = require("express-session");
const path = require("path")
dotenv.config()

const cookieParser=require("cookie-parser")

app.use(express.urlencoded({extended : true}));
app.use(cookieParser("somesecret"))
//Session middleware
app.use(session({
  secret: "somesecret",
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 24 * 60 * 60 * 1000,
  }
}));

const port = process.env.PORT || 5015

const Material= require("./routes/Material")
const User = require("./routes/User")
const StudyGroup = require("./routes/StudyGroup")
const MatRequest = require("./routes/MatRequest")
const StudyGroupJoin = require("./routes/StudyGroupJoin")
const db = require("./db/conn")


app.use(express.static(path.join(__dirname, "build")))
app.use(express.urlencoded({extended : true}));
app.use(express.json())
app.use(cors({
  origin:["http://88.200.63.148:3015"],
  methods:["GET", "POST"],
  credentials:true
}))


app.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname, "build", "index.html"))
})

app.use('/Material', Material);
app.use('/User', User);
app.use('/StudyGroup', StudyGroup);
app.use('/MatRequest', MatRequest);
app.use('/Joins', StudyGroupJoin);


///App listening on port
app.listen(port, ()=>{
    console.log("Server is running on port:" + port)
})